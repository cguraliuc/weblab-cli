const program = require('commander')

const configs = require('./config/config').all()
const { info, success, error } = require('./logger/logger')
const service = require('./utils/microservice')
const api = require('./api')

// Setup
program
  .arguments('[<microservice>]')
  .option('-r, --replicas [count]', 'replicas count [1]', 1)
  .description(`
    Scale up or down a deployed microservice.
  `)
  .action((microservice) => { program.microservice = microservice })
  .parse(process.argv)

const replicas = program.replicas

// Program args
const { microservice, version } = service(program)

if (Number.isNaN(replicas) || replicas < 0) {
  error('Replicas must be a number')
}

api
  .scale(Object.assign({}, configs, { microservice, version, replicas }))
  .then((body) => {
    info('')
    success(`${microservice}:${version} scaled to ${replicas} replicas`)
    info('')
  })
  .catch(e => error(e.message))
