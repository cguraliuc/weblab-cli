const fs = require('fs')
const path = require('path')
const program = require('commander')
const chalk = require('chalk')

const cli = require('./cli/cli')
const cmd = require('./command')
const config = require('./config/config')
const api = require('./api')
const { error, h2, p, warn } = require('./logger/logger')

// Setup
program
  .arguments('[<package>]')
  .description(`
    Deploy a microservice. If no arguments are supplied, Weblab CLI tries to deploy
    the current folder. <package> can be any of a folder, tarball, GitHub repo or
    Bitbucket repo.

    A microservice is in its simplest form a NPM package that exposes a function with
    the following signature in it's main entry file:

    ` +
    chalk.gray(`module.exports = function (req, res) {
      res.send('Hello, world!')
    }
  `))
  .option('-r, --replicas <count>', 'Deploy <count> replicas.')
  .option('--update', 'Update currently deployed service.')
  .option('-y, --yes', 'Auto accept confirmations.')
  .parse(process.argv)

const conf = config.all()

// Microservice package
const serviceFilePath = program.args[0] || '.'

// Number of instances
const replicas = Number.isNaN(parseInt(program.replicas, 10))
  ? 1
  : Math.max(parseInt(program.replicas, 10), 1)

/**
 * Get master tarball from GitHub repo url
 *
 * @param {String} url - GitHub repo url
 * @returns {String} - The tarball url
 */
const github = (pkg) => {
  const GITHUB_REGEX = /^https:\/\/github\.com/

  if (!GITHUB_REGEX.test(pkg)) {
    return false
  }

  let url

  url = pkg.slice(-1) === '/' ? pkg.slice(0, -1) : pkg
  url = url + '/archive/master.tar.gz'

  return url
}

/**
 * Get master tarball from BitBucket repo url
 *
 * @param {String} url - BitBucket repo url
 */
const bitbucket = (pkg) => {
  const BITBUCKET_REGEX = /^https:\/\/bitbucket\.org/

  if (!BITBUCKET_REGEX.test(pkg)) {
    return false
  }

  let url

  url = pkg.slice(-1) === '/' ? pkg.slice(0, -1) : pkg
  url = url + '/get/master.tar.gz'

  return url
}

const getPackage = (serviceFilePath) => {
  const filepath = path.resolve(process.cwd(), serviceFilePath)
  let stat

  try {
    stat = fs.statSync(filepath)
    // Silent error
  } catch (e) {}

  // Directory
  if (stat && stat.isDirectory()) {
    return cmd(`weblab build -s ${filepath}`)
      .then((archive) => ({
        file: archive.trim()
      }))
  }

  // GitHub repo
  const gh = github(serviceFilePath)
  if (gh) {
    return Promise.resolve({ url: gh })
  }

  // BitBucket repo
  const bb = bitbucket(serviceFilePath)
  if (bb) {
    return Promise.resolve({ url: bb })
  }

  return Promise.reject(new Error('Invalid folder, tarball, GitHub repo or BitBucket repo provided.'))
}

// Get confirmation
const confirm = () => cli
  .question('> Are you sure? (yes/no):')
  .then((answer) => answer === 'yes' || answer === 'y')

getPackage(serviceFilePath)
  .then(({ file, url }) => {
    let tarball
    let microservice
    let version
    let endpoints

    if (file) {
      const basename = path.basename(file, '.tar.gz')
      microservice = basename.slice(0, basename.lastIndexOf('-'))
      version = basename.slice(basename.lastIndexOf('-') + 1)
      tarball = fs.createReadStream(file).on('error', error)
      try {
        endpoints = require(path.resolve(process.cwd(), './routes')) || []
      } catch (e) {
        endpoints = [{
          method: 'GET',
          path: '/'
        }]
      }
    }

    return { url, tarball, microservice, version, endpoints }
  })
  .then(({ url, tarball, microservice, version, endpoints }) => {
    if (program.update && !program.yes) {
      process.stdout.write('\n')
      warn(`This will overwrite the deployed service ${microservice}:${version}`)
      process.stdout.write('\n')

      return confirm({ microservice, version })
        .then((confirmed) => {
          if (confirmed) {
          }

          throw new Error('cancelled')
        })
    }

    return { url, tarball, microservice, version, endpoints }
  })
  .then(({ url, tarball, microservice, version, endpoints }) => {
    process.stdout.write('\n')

    // Service name
    h2('Service:')
    p(`${microservice}:${version}` || url)

    // Scale
    if (program.replicas > 1) {
      h2('Scale:')
      p(`${program.replicas} replicas`)
    }

    // Endpoints
    process.stdout.write('\n')
    h2('Enpoints:')
    endpoints.forEach((endpoint) => {
      const service = `${conf.host}/api/v1/run/${conf.username}/${microservice}/${version}`
      const method = endpoint.method || 'GET'
      const path = endpoint.path
      const url = chalk.underline(`${service}${path}`)
      p(`${method} - ${url}`)
    })

    // Upload status
    process.stdout.write('\n')
    process.stdout.write(chalk.cyan('  Uploading: '))

    // Show progress
    const statusInterval = setInterval(() => process.stdout.write('.'), 1000)

    api.deploy({
      username: conf.username,
      token: conf.token,
      host: conf.host,
      file: tarball,
      url,
      replicas
    }).then(() => {
      // Clear progress
      clearInterval(statusInterval)

      process.stdout.write('\n')
      p(chalk.green('success'))
      process.stdout.write('\n')
    }).catch((e) => {
      // Clear progress
      clearInterval(statusInterval)

      p(chalk.red('error'))
      process.stdout.write('\n')

      error(e)
      process.exit(1)
    })
  })
  .catch((e) => {
    error(e.message)
    process.exit(1)
  })
