const exec = require('child_process').exec

// Run host command
module.exports = (cmd, options) => {
  options = options || {}
  options.cwd = options.cwd || process.cwd()

  return new Promise(function (resolve, reject) {
    exec(cmd, options, (error, stdout, stderr) => {
      if (error !== null) return reject(error)
      if (stderr) return reject(stderr)
      return resolve(stdout)
    })
  })
}
