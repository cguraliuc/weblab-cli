const path = require('path')
// const chalk = require('chalk')

module.exports = (program) => {
  if (program.microservice) {
    const parts = program.microservice.split(':')

    return {
      microservice: parts[0],
      version: parts[1]
    }
  }

  const directory = program.directory || '.'
  const dirname = path.resolve(process.cwd(), directory)
  let pkg = {}

  try {
    pkg = require(path.join(dirname, 'package.json'))
  } catch (e) {
    // console.error(chalk.red('Error'), e)
    // process.exit(1)
  }

  return {
    microservice: pkg.name,
    version: pkg.version
  }
}
