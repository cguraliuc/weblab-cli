const program = require('commander')
const chalk = require('chalk')

const config = require('./config/config')
const cli = require('./cli/cli')
const handlers = require('./api/handlers')
const { info, success, error } = require('./logger/logger')

// Setup
program
  .description(`
    Configure Weblab CLI with your email, username and access token.

    If you do not have a ${chalk.underline('https://weblab.io')} account yet, just run ${chalk.cyan('weblab register')} and setup
    your account. It's that easy and it takes only a few seconds.
  `)
  .parse(process.argv)

// Config
const conf = config.file() || {}

// Extract username from email
const extract = email => (email || '')
  .replace(/@.*$/, '')
  .replace(/[\W_]+/g, '')

// Enter email
const email = (opts) => {
  const saved = opts.email ? `(${opts.email})` : ''

  return cli
    .question(`> Email: ${saved}`, opts.email)
    .then(email => Object.assign({}, opts, { email }))
}

// Enter username
const username = (opts) => {
  const saved = opts.username ? opts.username : extract(opts.email)

  return cli
    .question(`> Username: (${saved})`, saved)
    .then(username => Object.assign({}, opts, { username }))
}

// Enter password
const token = (opts) => {
  let saved

  if (opts.token) {
    const hiddenToken = [
      opts.token.slice(0, 4),
      opts.token.slice(4, -4).replace(/(.+)/g, '************************'),
      opts.token.slice(-4)
    ].join('')

    saved = `(${hiddenToken})`
  } else {
    saved = ''
  }

  return cli
    .password(`> Token: ${saved}`, opts.token)
    .then(token => Object.assign({}, opts, { token }))
}

info('')
info('Setup Weblab CLI')
info('----------------')
info('')

email(conf)
  .then(username)
  .then(token)
  .catch(handlers.fatal)
  .then(config.save)
  .then(() => {
    success('Account setup complete.')
    info('')
  })
  .catch(e => error(e.message))
