const program = require('commander')
const chalk = require('chalk')

const configs = require('./config/config').all()
const { info, error } = require('./logger/logger')
const service = require('./utils/microservice')
const api = require('./api')
const cli = require('./cli/cli')

// Setup
program
  .arguments('[<microservice>]')
  .description(`
    List microservice workers. If no <microservice> is provided then will list all
    your currently deployed microservices workers.
  `)
  .action((microservice) => { program.microservice = microservice })
  .parse(process.argv)

// Program args
const { microservice, version } = service(program)

// Display microservices table
const results = (workers = []) => {
  const lengths = workers
    .map((val) => `${val.Microservice}:${val.Version}`.length)
    .concat('Microservice'.length)

  // Compute max width
  const max = Math.max.apply(null, lengths)

  // Show table
  info('')
  cli.table.header(max)
  workers.forEach((worker) => cli.table.row(worker, max))
  info('')
}

// Display empty message
const empty = (host) => {
  info('')
  info(`You dont't have any microservices deployed to ${chalk.underline(host)}`)
  info('Run \'weblab deploy\' and deploy your microservice. As simple as that.')
  info('')
}

// Get user microservice list
api
  .list(Object.assign({}, configs, { microservice, version }))
  .then((body) => {
    body.length ? results(body) : empty(configs.host)
  })
  .catch((err) => error(`${err.message}`))
