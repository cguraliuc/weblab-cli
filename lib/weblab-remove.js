const program = require('commander')

const configs = require('./config/config').all()
const { success, error } = require('./logger/logger')
const service = require('./utils/microservice')
const api = require('./api')

// Setup
program
  .arguments('[<microservice>]')
  .action((microservice) => { program.microservice = microservice })
  .parse(process.argv)

// Program args
const { microservice, version } = service(program)

api
  .remove(Object.assign({}, configs, { microservice, version }))
  .then(() => { success(`${microservice}:${version} removed`) })
  .catch((err) => error(`${err.message}`))
