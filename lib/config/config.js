const path = require('path')
const os = require('os')
const fs = require('fs')
const _merge = require('lodash.merge')

/**
 * Get saved user configs
 *
 * @param {String} filepath - The file path from where to load the config file. Defaults to
 * $HOME/.weblab/.config.json
 * @returns {Object} - The config object
 */
const file = (filepath) => {
  let confs = null

  try {
    confs = require(filepath || path.join(os.homedir(), '.weblab', 'config.json'))
  // silent error
  } catch (e) {}

  return confs
}

/**
 * Get configs from ENV variables
 *
 * @returns {Object} - The config object
 */
const env = () => ({
  host: process.env.WL_HOST,
  username: process.env.WL_USER,
  token: process.env.WL_TOKEN
})

/**
 * Get config options merged from all sources (file, env, etc.)
 *
 * @param {String} [filepath] - The file path from where to load the config file
 * @returns {Object} - The config object
 */
const all = (filepath) => _merge({ host: 'https://weblab.io' }, file(filepath), env())

/**
 * Save config options on file
 *
 * @param {Object} conf - The config options
 * @param {String} [filepath] - The file path where to store the config file. Defaults to
 * $HOME/.weblab/.config.json
 * @returns {Void}
 */
const save = (conf, filepath) => {
  const file = path.join(filepath || path.join(os.homedir(), '.weblab', 'config.json'))
  const data = JSON.stringify(conf, null, 2)

  return fs.writeFileSync(file, data)
}

module.exports = {
  all,
  env,
  file,
  save
}
