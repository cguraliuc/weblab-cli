const program = require('commander')

const server = require('./local/server')

// Setup
program
  .description(`
    Run your microservice locally. Usefull for local development.
  `)
  .option('-p, --port <number>', 'The port on which to start to local server', 8991)
  .parse(process.argv)

const port = process.env.WL_LOCAL_PORT || program.port

// Start server
server({ port })
