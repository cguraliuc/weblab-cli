const c = require('chalk')
const read = require('read')
const padEnd = require('lodash.padend')
const dateFormat = require('date-fns/format')

const table = {
  header: (max) => console.log(c.cyan([
    padEnd('ID', 14),
    padEnd('MICROSERVICE', max),
    padEnd('WORKERS', 7),
    padEnd('CREATED AT', 19),
    padEnd('UPDATED AT', 19)
  ].join(' | '))),

  row: (worker, max) => console.log([
    padEnd(worker.Id.slice(0, 12), 14),
    padEnd(`${worker.Microservice}:${worker.Version}`, max),
    padEnd(worker.Workers, 7),
    padEnd(dateFormat(worker.CreatedAt, 'YYYY-MM-DD HH:mm:ss'), 19),
    padEnd(dateFormat(worker.UpdatedAt, 'YYYY-MM-DD HH:mm:ss'), 19)
  ].join(' | '))
}

/**
 * Password input
 *
 * @param {String} text - Text to be prompted
 * @param {String} [defaultAnswer] - Default answer if none is provided by user
 * @return {Promise}
 */
const password = (text, defaultAnswer) => new Promise((resolve, reject) => {
  read({ prompt: text, silent: true, replace: '*' }, (err, res) => {
    err ? reject(err) : resolve(res !== '' ? res : defaultAnswer)
  })
})

/**
 * Question input
 *
 * @param {String} text - Text to be prompted
 * @param {String} [defaultAnswer] - Default answer if none is provided by user
 * @return {Promise}
 */
const question = (text, defaultAnswer) => new Promise((resolve, reject) => {
  read({ prompt: text }, (err, res) => {
    err ? reject(err) : resolve(res !== '' ? res : defaultAnswer)
  })
})

module.exports = {
  table,
  question,
  password
}
