const path = require('path')
const app = require('express')()
const bodyParser = require('body-parser')
const c = require('chalk')
const fs = require('fs')
const yaml = require('js-yaml')

const { info, h2, p } = require('../logger/logger')

/**
 * Weblab.io supports only these HTTP methods
 */
const ALLOWED_METHODS = [
  'delete',
  'get',
  'head',
  'options',
  'patch',
  'post',
  'put'
]

/**
 * Middlewares for parsing request bodies
 */
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

/**
 * Load routes configuration from routes.json file
 *
 * @param {String} [filepath='routes.json'] - The routes config file path
 * @returns {Array.<Object>} - The routes config
 */
const fromRoutesFile = (filepath) => {
  let routes = []

  // Try to load routes from routes.json
  try {
    routes = require(filepath || path.resolve(process.cwd(), 'routes.json'))
  } catch (e) {
    // If we can't load routes.json just skip to next loader method
  }

  return routes
}

/**
 * Load config from weblab.yml file
 *
 * @param {String} dir - The directory where to try to load the yml config
 * @return {Array} - Returns the config object
 */
const fromYaml = (dir) => {
  const routes = []

  // Load from weblab.yml
  try {
    const config = yaml.safeLoad(fs.readFileSync(path.join(dir, 'weblab.yml'), 'utf8'))

    Object.keys(config.routes).forEach((path) => {
      const methods = Object.keys(config.routes[path])

      methods.forEach((m) => {
        const method = m.toUpperCase()
        const handler = config.routes[path][m]

        routes.push({ method, path, handler })
      })
    })

    return routes
  } catch (e) {}

  return routes
}

/**
 * Load routes configuration from package.json file. If no `main` entry file is specified in
 * `package.json`, then it will default to `index.js`.
 *
 * @returns {Array.<Object>} - The routes config
 */
const fromPackageFile = () => {
  const pkg = require(path.resolve(process.cwd(), 'package.json'))
  const main = pkg.main || 'index.js'

  return [
    { method: 'GET', path: '/', handler: main }
  ]
}

/**
 * Run local weblab.io server
 *
 * @param {Object} options - The configuration options
 * @param {Number} options.port - The port number on which to start the server
 * @param {String} options.file - The routes config file
 */
const server = ({
  port = process.env.WL_LOCAL_PORT || 8991,
  file = 'routes.json'
} = {}) => {
  const routesJsonFile = path.resolve(process.cwd(), file)

  // Try to load routes from routes.json
  let routes = fromRoutesFile(routesJsonFile)
  // If no routes file present try to load from weblab.yml
  if (!routes.length) routes = fromYaml(process.cwd())
  // If no routes file present default to package main entry or index.js
  if (!routes.length) routes = fromPackageFile()

  const pkg = require(path.resolve(process.cwd(), 'package.json'))
  const service = `${pkg.name}-${pkg.version}`

  h2('')
  h2('Service:')
  p(service)

  h2('')
  h2('Endpoints:')

  routes.forEach((route) => {
    const method = route.method || 'GET'

    if (ALLOWED_METHODS.indexOf(method.toLowerCase()) !== -1) {
      const handler = require(path.resolve(process.cwd(), route.handler))
      const url = `http://localhost:${port}${route.path}`

      p(`${method} - ${c.underline(url)}`)

      app[method.toLowerCase()](route.path, handler)
    }
  })

  info('')

  app.listen(port, () => {
    h2('Server:')
    p(`Started on port ${port}`)
    p('')
  })
}

module.exports = server
