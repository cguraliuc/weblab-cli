const path = require('path')
const fs = require('fs')
const archiver = require('archiver')
const program = require('commander')
const chalk = require('chalk')
const glob = require('glob-all')
const { info, error, p } = require('./logger/logger')

const DEFAULT_FILES = [
  '**',       // Include all files
  '!.weblab', // Exlcude .weblab
  '!.git'     // Exclude .git
]

// Setup
program
  .description('' +
  `Create a tarball from a microservice in the current directory.

  A microservice is in its simplest form a NPM package that exposes a function with the following
  signature in it's main entry file:

  ` +

  chalk.gray(`module.exports = (req, res) => {
    res.send('Hello, world!')
  }`)
  )
  .option('-s, --silent')
  .parse(process.argv)

let pkg

try {
  pkg = require(path.resolve('.', 'package.json'))
} catch (e) {
  error(e.message)
  process.exit(1)
}

const filename = `${pkg.name}-${pkg.version}.tar.gz`
const weblabDirectory = path.join('.', '.weblab')
const target = path.join(weblabDirectory, filename)

// Create .weblab directory if id doesn't exists already
if (!fs.existsSync(weblabDirectory)) {
  fs.mkdirSync(weblabDirectory)
}

// Create a file to stream archive data to.
const output = fs.createWriteStream(target)
const archive = archiver('tar', { gzip: true })

let buildInterval

// Listen for all archive data to be written
output.on('close', () => {
  if (buildInterval) {
    clearInterval(buildInterval)
  }

  if (program.silent) {
    return console.log(target)
  }

  info('')
  p(target)
  info('')
})

// Catch errors
archive.on('error', (err) => {
  info('')
  error(err)
  info('')
})

output.on('open', () => {
  // Upload status
  if (!program.silent) {
    process.stdout.write('\n')
    process.stdout.write(chalk.cyan('  Building: '))
    buildInterval = setInterval(() => process.stdout.write('.'), 1000)
  }

  // Pipe archive data to the file
  archive.pipe(output)

  // Find files to add to archive
  const files = glob.sync(DEFAULT_FILES, {
    cwd: process.cwd(),
    dot: true,
    silent: true,
    follow: true
  })

  files.forEach((filePath) => {
    const fullPath = path.resolve(
      process.cwd(),
      filePath
    )

    const stats = fs.statSync(fullPath)

    if (!stats.isDirectory(fullPath)) {
      archive.append(fs.readFileSync(fullPath), {
        name: filePath,
        mode: stats.mode
      })
    }
  })

  // Finalize the archive
  archive.finalize()
})
