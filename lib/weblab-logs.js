const program = require('commander')
const EventSource = require('eventsource')
const chalk = require('chalk')
const ms = require('ms')

const configs = require('./config/config').all()
const { info, error } = require('./logger/logger')
const service = require('./utils/microservice')
const api = require('./api')

// Setup
program
  .arguments('[<microservice>]')
  .option('-f, --follow', 'Follow log output')
  .option('-t, --tail <count>', 'Limit number or rows returned')
  .option('-l, --last <timespan>', 'Logs in the defined timespan. Ex: 10s, 12h, 3d.')
  .action((microservice) => { program.microservice = microservice })
  .parse(process.argv)

// Program args
const { microservice, version } = service(program)
const tail = program.tail
const last = program.last ? ms(program.last) : undefined

const stream = () => {
  const es = new EventSource(`https://logs.weblab.io/${configs.username}/${microservice}/${version}/stream`, {
    headers: {
      'Authorization': `JWT ${configs.token}`
    }
  })

  let firsttime = true

  es.on('open', function open () {
    if (firsttime) {
      firsttime = false

      const service = `${microservice}:${version}`
      const now = new Date()
      const time = `[${now.toString()}]`

      info('')
      console.log(chalk.cyan(time), 'Connected to streaming logs:', service)
      info('')
    }
  })

  es.on('message', function incoming (event) {
    const now = new Date()
    const time = `[${now.toString()}]`
    console.log(chalk.cyan(time), event.data)
  })

  const shutdown = () => {
    es.close()
    setTimeout(() => process.exit(0), 500)
  }

  process
    .on('SIGTERM', shutdown)
    .on('SIGINT', shutdown)
}

if (program.follow) {
  stream()
} else {
  // Get microservice logs
  let request

  // Last
  if (program.last) request = api.logs(Object.assign({}, configs, { microservice, version, since: Date.now() - last }))
  else request = api.logs(Object.assign({}, configs, { microservice, version, tail }))

  request
    .then((body) => {
      body.forEach((line) => {
        const time = `[${(new Date(line.time)).toString()}]`
        const host = `[${line.container_id.slice(0, 12)}]`
        console.log(chalk.cyan(time), chalk.gray(host), line.log)
      })
    })
    .catch((err) => error(`${err.message}`))
}
