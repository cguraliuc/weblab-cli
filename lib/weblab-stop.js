const program = require('commander')

const configs = require('./config/config').all()
const api = require('./api')
const service = require('./utils/microservice')
const { info, success, error } = require('./logger/logger')

// Setup
program
  .arguments('[<microservice>]')
  .description(`
    Stop a running microservice.
  `)
  .action((microservice) => { program.microservice = microservice })
  .parse(process.argv)

// Program args
const { microservice, version } = service(program)

api
  .scale(Object.assign({}, configs, { microservice, version, replicas: 0 }))
  .then((body) => {
    info('')
    success(`${microservice}:${version} stopped`)
    info('')
  })
  .catch(e => error(e.message))
