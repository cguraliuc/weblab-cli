const c = require('chalk')

const log = console.log.bind(console, '  ')
const info = console.log.bind(console, c.cyan(' '))
const success = console.log.bind(console, c.green('  Success:'))
const warn = console.log.bind(console, c.yellow('  Warning:'))
const error = console.log.bind(console, c.red('  Error:'))

const h2 = m => console.log(c.cyan(`  ${m}`))
const p = m => console.log(`    ${m}`)

module.exports = {
  log,
  info,
  success,
  warn,
  error,
  h2,
  p
}
