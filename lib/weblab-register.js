const program = require('commander')

const config = require('./config/config')
const cli = require('./cli/cli')
const api = require('./api')
const handlers = require('./api/handlers')
const { info, success, error } = require('./logger/logger')

// Setup
program
  .description(`
    Register a new account with your desired email and password.
  `)
  .parse(process.argv)

// Extract username from email
const extract = email => (email || '')
  .replace(/@.*$/, '')
  .replace(/[\W_]+/g, '')

// Enter email
const email = () => cli
  .question('> Email: ')
  .then(email => ({ email }))

// Enter username
const username = (opts) => cli
  .question(`> Username: (${extract(opts.email)})`, extract(opts.email))
  .then(username => Object.assign({}, opts, { username }))

// Enter password
const password = (opts) => cli
  .password('> Password: ')
  .then(password => Object.assign({}, opts, { password }))

// Register a new account
const register = (conf) => {
  const host = config.all().host
  const data = Object.assign({}, { host }, conf)

  return api
    .register(data)
    .then((body) => ({
      username: body.username,
      token: body.token,
      email: conf.email,
      host
    }))
}

info('')
info('Setup your Weblab Account')
info('-------------------------')

email()
  .then(username)
  .then(password)
  .catch(handlers.fatal)
  .then(register)
  .then(config.save)
  .then(() => { success('User registration complete.') })
  .catch(e => error(e.message))
