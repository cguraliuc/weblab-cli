/**
 * @module lib/api/remove
 */

const request = require('superagent')
const handlers = require('./handlers')

/**
 * Remove deployed microservice from Weblab
 *
 * @param {Object} params - The remove params
 * @param {String} params.host - The host url (http://weblab.io)
 * @param {String} params.token - The authentication token
 * @param {String} params.username - The account username
 * @param {String} params.microservice - The microservice name
 * @param {String} params.version - The microservice version
 * @returns {Promise}
 */
const remove = ({
  host,
  token,
  username,
  microservice,
  version
}) => {
  return request
    .del(`${host}/api/v1/users/${username}/services/${microservice}/${version}`)
    .set('Authorization', 'JWT ' + token)
    .then(handlers.success)
    .catch(handlers.error)
}

module.exports = remove
