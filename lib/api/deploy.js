/**
 * @module lib/api/deploy
 */

const request = require('superagent')
const handlers = require('./handlers')

/**
 * Deploy a microservice to Weblab
 *
 * @param {Object} params - The deploy params
 * @param {String} params.host - The host url http://weblab.io
 * @param {String} params.token - The authentication token
 * @param {String} params.username - The account username
 * @param {String} [params.file] - The file path of the microservice package
 * @param {String} [params.url] - The url of the microservice package
 * @param {Integer} [params.replicas=1] - The number of microservice instances to deploy
 * @returns {Promise}
 */
const deploy = ({
  host,
  token,
  username,
  file,
  url,
  replicas = 1
}) => {
  const req = request
    .post(`${host}/api/v1/users/${username}/services`)
    .set('Authorization', 'JWT ' + token)
    .field('replicas', replicas)

  if (url) req.field('url', url)
  if (file) req.attach('file', file)

  return req
    .then(handlers.success)
    .catch(handlers.error)
}

module.exports = deploy
