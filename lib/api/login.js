/**
 * @module lib/api/login
 */

const request = require('superagent')
const handlers = require('./handlers')

/**
 * Get an access token
 *
 * @param {Object} params - The login params
 * @param {String} params.host - The host url (http://weblab.io)
 * @param {String} params.email - The account email
 * @param {String} params.password - The account password
 * @returns {Promise}
 */
const login = ({
  email,
  password,
  host
}) => {
  return request
    .post(`${host}/api/v1/account/login`)
    .send({ email, password })
    .then(handlers.success)
    .catch(handlers.error)
}

module.exports = login
