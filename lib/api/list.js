/**
 * @module lib/api/list
 */

const request = require('superagent')
const handlers = require('./handlers')

/**
 * List microservices deployed to Weblab
 *
 * @param {Object} params - The list params
 * @param {String} params.host - The host url (http://weblab.io)
 * @param {String} params.token - The authentication token
 * @param {String} params.username - The account username
 * @param {String} params.microservice - The microservice name
 * @returns {Promise}
 */
const list = ({
  host,
  token,
  username,
  microservice
}) => {
  return request
    .get(`${host}/api/v1/users/${username}/services`)
    .set('Authorization', 'JWT ' + token)
    .query({ microservice })
    .then(handlers.success)
    .catch(handlers.error)
}

module.exports = list
