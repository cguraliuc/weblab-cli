/**
 * @module lib/api/scale
 */

const request = require('superagent')
const handlers = require('./handlers')

/**
 * Scale up or down a microservice deployed to Weblab
 *
 * @param {Object} params - The deploy params
 * @param {String} params.host - The host url (http://weblab.io)
 * @param {String} params.token - The authentication token
 * @param {String} params.username - The account username
 * @param {String} params.microservice - The microservice name
 * @param {String} params.version - The microservice version
 * @param {Integer} params.replicas - The number of replicas
 * @returns {Promise}
 */
const list = ({
  host,
  token,
  username,
  microservice,
  version,
  replicas
}) => {
  return request
    .post(`${host}/api/v1/users/${username}/services/${microservice}/${version}/scale`)
    .set('Authorization', 'JWT ' + token)
    .send({ replicas })
    .then(handlers.success)
    .catch(handlers.error)
}

module.exports = list
