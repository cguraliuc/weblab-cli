/**
 * @module lib/api/handlers
 */

const log = require('../logger/logger')

/**
 * Handle superagent success responses. Return response body.
 *
 * @param {Object} res - The response object
 * @returns {*} - The response body
 */
const success = (res) => res.body

/**
 * Handle superagent error responses. If it is an http error return code + message,
 * else return initial error.
 *
 * @param {Error} err - The error object
 * @returns {Error} - The error object
 */
const error = (err) => {
  if (err && err.response && err.response.body && err.response.body.code) {
    const body = err.response.body
    throw new Error(`${body.code} - ${body.message}`)
  }

  if (err.status) throw new Error(`${err.status} - ${err.message}`)

  throw err
}

/**
 * Handle fatal errors. Prints error to stadout and then exist with status code.
 *
 * @param {Error} err - The error object
 * @param {Integer} code - The status code
 * @returns {Error} - The error object
 */
const fatal = (err, code) => {
  process.stdout.write('\n')
  log.error(err.message)
  process.exit(code || 1)
}

module.exports = {
  success,
  error,
  fatal
}
