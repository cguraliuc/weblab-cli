module.exports = {
  deploy: require('./deploy'),
  list: require('./list'),
  login: require('./login'),
  logs: require('./logs'),
  register: require('./register'),
  remove: require('./remove'),
  scale: require('./scale')
}
