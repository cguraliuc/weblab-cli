/**
 * @module lib/api/logs
 */

const request = require('superagent')
const handlers = require('./handlers')

const logs = ({
  username,
  microservice,
  version,
  since,
  tail,
  stdout,
  stderr,
  host,
  token
}) => {
  return request
    .get(`https://logs.weblab.io/${username}/${microservice}/${version}`)
    .set('Authorization', 'JWT ' + token)
    .query({ stdout, stderr, since, tail })
    .then(res => res.body)
    .catch(handlers.error)
}

module.exports = logs
