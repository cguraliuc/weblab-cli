/**
 * @module lib/api/sign
 */

const request = require('superagent')
const handlers = require('./handlers')

/**
 * Get an upload url for a microservice
 *
 * @param {Object} params - The deploy params
 * @param {String} params.host - The host url http://weblab.io
 * @param {String} params.token - The authentication token
 * @param {String} params.username - The account username
 * @param {String} params.microservice - The authentication token
 * @param {Integer} [params.replicas=1] - The number of replicas
 * @returns {Promise}
 */
const sign = ({
  host,
  token,
  username,
  microservice,
  scale = 1
}) => {
  return request
    .post(`${host}/api/v1/users/${username}/sign`)
    .set('Authorization', 'JWT ' + token)
    .send({ microservice, scale })
    .then(handlers.success)
    .catch(handlers.error)
}

module.exports = sign
