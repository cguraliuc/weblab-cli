/**
 * @module lib/api/register
 */

const request = require('superagent')
const handlers = require('./handlers')

/**
 * Register a new account
 *
 * @param {Object} params - The register params
 * @param {String} params.host - The host url (http://weblab.io)
 * @param {String} params.email - The account email
 * @param {String} params.username - The account username
 * @param {String} params.password - The account password
 * @returns {Promise}
 */
const register = ({
  email,
  username,
  password,
  host
}) => {
  return request
    .post(`${host}/api/v1/account/register`)
    .send({ email, username, password })
    .then(handlers.success)
    .catch(handlers.error)
}

module.exports = register
