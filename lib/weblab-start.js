const program = require('commander')

const configs = require('./config/config').all()
const api = require('./api')
const service = require('./utils/microservice')
const { info, success, error } = require('./logger/logger')

// Setup
program
  .arguments('[<microservice>]')
  .option('-r, --replicas [count]', 'replicas count [1]', 1)
  .description(`
    Start a stopped microservice.
  `)
  .action((microservice) => { program.microservice = microservice })
  .parse(process.argv)

const replicas = program.replicas

// Program args
const { microservice, version } = service(program)

api
  .scale(Object.assign({}, configs, { microservice, version, replicas }))
  .then((body) => {
    info('')
    success(`${microservice}:${version} started`)
    info('')
  })
  .catch(e => error(e.message))
