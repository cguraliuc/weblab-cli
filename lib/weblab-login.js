const program = require('commander')

const cli = require('./cli/cli')
const config = require('./config/config')
const api = require('./api')
const handlers = require('./api/handlers')
const { info, success, error } = require('./logger/logger')

// Setup
program
  .description(`
    Get an access token in order to be able to access private APIs on Weblab. You
    need to provide your email and password in order to receive your access token.

    If you do not have yet an account registered with Weblab than this will try to
    register a new account with the email and password provided.
  `)
  .parse(process.argv)

// Enter email
const email = () => cli
  .question('> Email: ')
  .then((email) => ({ email }))

// Enter password
const password = ({ email }) => cli
  .password('> Password: ')
  .then((password) => ({ email, password }))

// Login to weblab.io
const login = (conf) => {
  const host = config.all().host
  const data = Object.assign({}, { host }, conf)

  return api
    .login(data)
    .then((body) => ({
      username: body.username,
      token: body.token,
      email: conf.email,
      host
    }))
}

info('')
info('Login with your Weblab Account')
info('------------------------------')
info('')

email()
  .then(password)
  .catch(handlers.fatal)
  .then(login)
  .then(conf => config.save(conf))
  .then(() => { success('You can now use the CLI to access weblab.io services') })
  .catch(e => error(e.message))
