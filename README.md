# Weblab CLI

**[Weblab](https://weblab.io)** – Build applications comprised of microservices that run forever, self-heal and scale as needed. Building scalable fault-tolerant systems has never been easier.

The **Weblab CLI** it's a command-line tool to interact with [Weblab](https://weblab.io/) for developing and deploying your serverless microservice architecture.

## Contents

*   [Quick Start](#quick-start)
*   [Examples](#examples)
*   [Upcoming Features](#features)
*   [Development](#development)

## <a name="quick-start"></a>Quick Start

Follow the steps below to create and deploy your first serverless microservice in seconds.

1.  **Install via npm:**

    ```
    $ npm install -g weblab-cli
    ```

2.  **Set-up your credentials**

    ```
    $ weblab register

      Setup your Weblab Account
      -------------------------
    > Email: cristi@weblab.io
    > Username: (cristi)
    > Password: ***********

      Success: User registration complete.
    ```

    or if you already have a Weblab account just sign in with your credentials

    ```
    $ weblab login
    ```

3.  **Deploy a service**

    In your Weblab service folder just run

    ```
    $ cd service
    $ wl deploy

      Service:
        hello-world:1.0.0

      Enpoints:
        GET - https://weblab.io/api/v1/run/cristi/hello-world/1.0.0/

      Uploading: ...
        success

    ```

4.  **List your services**

    ```
    $ wl ls

    CONTAINER      | MICROSERVICE           | STATE      | STATUS    
    3a377aea233d   | hello-world:1.0.0      | running    | Up 2 hours
    3a48678fce52   | hal:1.0.0              | running    | Up 2 hours
    8bd1f4577575   | container-events:0.0.2 | running    | Up 3 weeks
    1c260895c73e   | weblab-fb-bot:1.0.0    | running    | Up 11 weeks

    ```

5.  **Inspect logs**

    ```
    $ wl logs <service>
    ```

6.  **Realtime log streaming**

    ```
    $ wl logs -f <service>
    ```

    ```
    [15:00:39 GMT+0200 (EET)] Connected to streaming logs: hal:1.0.1

    [15:00:51 GMT+0200 (EET)] [9805924c6080]: It can only be attributable to human error.
    [15:01:16 GMT+0200 (EET)] [591663e757b8]: Affirmative, Dave. I read you.
    ```

7.  **Remove a container**

    ```
    $ wl rm <service>
    ```

8.  **Scale your service**

    ```
    $ wl deploy -r 3

      Service:
        hello-world:1.0.0
      Scale:
        3 workers

      Enpoints:
        GET - https://weblab.io/api/v1/run/cristi/hello-world/1.0.0/

      Uploading: ...
        success

    $ wl ls

      CONTAINER      | MICROSERVICE           | WORKERS | STATE      | STATUS
      6b0c33c7f206   | hello-world:1.0.0      | 3       | running    | Up 4 seconds
      3a48678fce52   | hal:1.0.0              | 1       | running    | Up 3 hours
      8bd1f4577575   | container-events:0.0.2 | 1       | running    | Up 3 weeks
      1c260895c73e   | weblab-fb-bot:1.0.0    | 1       | running    | Up 11 weeks

    ```

9.  **Run your service on your local machine**

    ```
    $ wl local

      Service:
        hello-world:1.0.0

      Endpoints:
        GET - http://localhost:8991/
        POST - http://localhost:8991/

      Server:
        Started on port 8991

    ```

    Useful if you want to test your service locally before deploying to Weblab.


## <a name="examples"></a>Examples

### Hello World

*package.json*
```
{
  "name": "hello-world",
  "version": "1.0.0",
  "main": "index.js"
}
```

*index.js*
```
module.exports = function (req, res) {
  res.send('Hello, world!')
}
```


## <a name="features"></a>Upcoming Features

1.  **Remote debugging** (scheduled for February 2017)

    Use your favorite IDE (WebStorm / VS Code / etc.) to remote debug your service.


## <a name="development"></a>Development

### Tests

```
$ npm test
```

#### Linter

We're using [ESLint](http://eslint.org/) with [Standard](https://github.com/feross/eslint-config-standard) config

[![js-standard-style](https://cdn.rawgit.com/feross/standard/master/badge.svg)](http://standardjs.com)

```
$ npm run lint
```

#### Unit tests

```
$ npm run unit
```

### Git Hooks

-   **pre-commit** hook to run *linter* before every commit

To install it just run in the **project root directory**:

```
$ ./tools/hooks/install.sh
```

If you want to skip the tests when you make a push just use the `--no-verify` flag. Example:

```
$ git commit --no-verify <remote> <branch>
```
