const os = require('os')
const fs = require('fs')
const path = require('path')
const expect = require('expect.js')

const microservice = require('../../lib/utils/microservice')

describe('utils', function () {
  describe('microservice', function () {
    it('should return microservice if is already defined', () => {
      const expected = { microservice: 'my-mycroservice', version: '1.0.0' }
      const actual = microservice({ microservice: 'my-mycroservice:1.0.0' })

      expect(actual).to.be.eql(expected)
    })

    it('should read package.json file from directory', () => {
      const payload = {
        name: 'my-mycroservice',
        version: '1.0.0'
      }
      const tmpdir = fs.mkdtempSync(os.tmpdir() + path.sep)
      const file = path.join(tmpdir, 'package.json')
      fs.writeFileSync(file, JSON.stringify(payload))

      const expected = { microservice: 'my-mycroservice', version: '1.0.0' }
      const actual = microservice({ directory: tmpdir })

      expect(actual).to.be.eql(expected)

      fs.unlinkSync(file)
      fs.rmdirSync(tmpdir)
    })
  })
})
