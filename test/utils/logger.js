const expect = require('expect.js')

const logger = require('../../lib/logger/logger')

describe('utils', function () {
  describe('logger', function () {
    it('should have all the api methods defined', () => {
      ['log', 'info', 'success', 'warn', 'error'].forEach((method) => {
        expect(logger[method]).to.be.a('function')
      })
    })
  })
})
