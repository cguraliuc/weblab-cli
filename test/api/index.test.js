const expect = require('expect.js')

const api = require('../../lib/api')

const apiMethods = [
  'deploy',
  'list',
  'login',
  'logs',
  'register',
  'remove',
  'scale'
]

describe('api', function () {
  const app = require('express')()
  const PORT = 6033
  const host = `http://localhost:${PORT}`
  let server

  before((done) => {
    app.all('/api*', (req, res) => { res.send({ message: 'OK' }) })
    server = app.listen(PORT, () => done())
  })

  after((done) => server.close(done))

  describe('index', function () {
    it('should have all the api methods defined', () => {
      apiMethods.forEach((method) => {
        expect(api[method]).to.be.a('function')
      })
    })

    it('should return promise when invoking an api method', () => {
      apiMethods.forEach((method) => {
        expect(api[method]({ host: `${host}/api` }).then).to.be.a('function')
      })
    })

    it('should not return error when status code < 400', (done) => {
      api.list({ host })
        .then(res => {
          expect(res).to.be.eql({ message: 'OK' })
          done()
        })
        .catch(done)
    })

    it('should return error when status code >= 400', (done) => {
      api.list({ host: `${host}/not-found` })
        .then((res) => done(new Error('Should not get here')))
        .catch((err) => {
          expect(err).to.be.ok()
          done()
        })
    })
  })
})
