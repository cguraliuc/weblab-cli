const fs = require('fs')
const path = require('path')
const expect = require('expect.js')

const config = require('../../lib/config/config')

describe('config', () => {
  describe('file', () => {
    // Config file path
    const filepath = path.resolve(__dirname, 'config.test.json')

    before(() => {
      const conf = {
        host: 'https://weblab.io',
        user: 'test_01422',
        token: 'test_token_34729'
      }

      fs.writeFileSync(filepath, JSON.stringify(conf))
    })

    after(() => fs.unlinkSync(filepath))

    it('should load configs from config file', () => {
      expect(config.file(filepath)).to.be.eql({
        host: 'https://weblab.io',
        user: 'test_01422',
        token: 'test_token_34729'
      })
    })
  })

  describe('env', () => {
    before(() => {
      process.env.WL_HOST = 'https://weblab.io'
      process.env.WL_USER = 'some_test_user'
      process.env.WL_TOKEN = 'some_test_token'
    })

    after(() => {
      delete process.env.WL_HOST
      delete process.env.WL_USER
      delete process.env.WL_TOKEN
    })

    it('should load configs from ENV variables', () => {
      expect(config.env()).to.be.eql({
        host: 'https://weblab.io',
        username: 'some_test_user',
        token: 'some_test_token'
      })
    })
  })

  describe('mixed', () => {
    // Config file path
    const filepath = path.resolve(__dirname, 'mixed.test.json')

    before(() => {
      process.env.WL_TOKEN = 'test_token_57439'
      fs.writeFileSync(filepath, JSON.stringify({
        user: 'test_54785'
      }))
    })

    after(() => {
      delete process.env.WL_TOKEN
      fs.unlinkSync(filepath)
    })

    it('should load configs from both ENV variables and config file', () => {
      expect(config.all(filepath)).to.be.eql({
        host: 'https://weblab.io',
        user: 'test_54785',
        token: 'test_token_57439'
      })
    })
  })
})
