const expect = require('expect.js')

const cli = require('../../lib/cli/cli')

describe('cli', () => {
  it('should have all methods defined', () => {
    ['question', 'password'].forEach(m => expect(cli[m]).to.be.a('function'))
  })
})
