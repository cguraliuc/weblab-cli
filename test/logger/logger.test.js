const expect = require('expect.js')

const logger = require('../../lib/logger/logger')

describe('logger', () => {
  it('should have all methods defined', () => {
    [
      'log',
      'info',
      'success',
      'warn',
      'error',

      'h2',
      'p'
    ].forEach(m => expect(logger[m]).to.be.a('function'))
  })
})
