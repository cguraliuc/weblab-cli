#!/usr/bin/env bash

# Install git hooks
cp -i tools/hooks/pre-commit .git/hooks/pre-commit
